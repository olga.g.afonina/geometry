﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Figures
{
	/// <summary>
	/// Класс, описывающий прямоугольник
	/// </summary>
	public class Rectangle : IFigure
	{
		/// <summary>
		/// Тип фигуры
		/// </summary>
		public string Type => "Прямоугольник";

		/// <summary>
		/// Высота
		/// </summary>
		public double Height { get; private set; }

		/// <summary>
		/// Ширина
		/// </summary>
		public double Width { get; private set; }

		public Rectangle(double height, double width)
		{
			if (height <= 0 || width <= 0)
			{
				throw new ArgumentOutOfRangeException("Сторона прямоугольника должна быть больше нуля");
			}
			Height = height;
			Width = width;
		}

		/// <summary>
		/// Вычисление площади прямоугольника
		/// </summary>
		/// <returns>Площадь прямоугольника</returns>
		public double GetSquare()
		{
			return Height * Width;
		}
	}
}
