﻿namespace Figures
{
	public interface IFigure
	{
		string Type { get; }
		double GetSquare();
	}
}
