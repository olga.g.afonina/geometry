﻿using System;

namespace Figures
{
	public class Circle : IFigure
	{
		public string Type { get => "Круг"; }
		public double Radius { get; private set; }

		public Circle(double r)
		{
			if (r <= 0)
			{
				throw new ArgumentOutOfRangeException("Radius: Радиус должен быть положительным");
			}
			Radius = r;
		}
		public double GetSquare()
		{
			return Math.PI * Radius * Radius;
		}
	}
}
