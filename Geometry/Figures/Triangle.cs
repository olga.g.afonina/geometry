﻿using System;

namespace Figures
{
	/// <summary>
	/// Класс, описывающий треугольник
	/// </summary>
	public class Triangle : IFigure
	{
		/// <summary>
		/// Тип фигуры
		/// </summary>
		public string Type => "Треугольник";

		/// <summary>
		/// Стороны треугольника
		/// </summary>
		public double A { get; private set; }
		public double B { get; private set; }
		public double C { get; private set; }

		public Triangle(double a, double b, double c)
		{
			if (a <= 0 || b <= 0 || c <= 0)
			{
				throw new ArgumentOutOfRangeException("Сторона треугольника не может быть отрицательной");
			}
			A = a;
			B = b;
			C = c;
		}

		/// <summary>
		/// Вычисление площади треугольника
		/// </summary>
		/// <returns>Площадь треугольника</returns>
		public double GetSquare()
		{
			var p = GetPerimeter() / 2;
			return Math.Sqrt(p * (p - A) * (p - B) * (p - C));
		}

		/// <summary>
		/// Вычисление периметра треугольника
		/// </summary>
		/// <returns>Периметр треугольника</returns>
		public double GetPerimeter()
		{
			return A + B + C;
		}

		/// <summary>
		/// Является ли треугольник прямоугольным?
		/// </summary>
		public bool IsRectangular()
		{
			return (A * A + B * B == C * C) || (A * A + C * C == B * B) || (C * C + B * B == A * A);
		}
	}
}