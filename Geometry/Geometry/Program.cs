﻿using System;
using System.Collections.Generic;
using Figures;

namespace Geometry
{
	class Program
	{
		static void Main(string[] args)
		{
			var list = new List<IFigure>();
			list.Add(new Rectangle(3, 5));
			list.Add(new Circle(5.2));
			list.Add(new Triangle(1, 2, 3));

			foreach (var l in list)
			{
				Console.WriteLine($"{l.Type}, площадь: {l.GetSquare():f2}");
			}

			Console.ReadLine();
		}
	}
}
