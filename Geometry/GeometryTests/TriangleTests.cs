﻿using Figures;
using NUnit.Framework;
using System;

namespace GeometryTests
{
	class TriangleTests
	{
		[Test]
		[TestCase(-5, 1, 2)]
		[TestCase(5, -1, 2)]
		[TestCase(5, 1, -2)]
		[TestCase(0, 0, 0)]
		public void Tringle__WhenABCIsLessOrEqualZero_ShouldThrowArgumentOutOfRange(double a, double b, double c)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() => new Triangle(a, b, c));
		}

		[Test]
		[TestCase(3, 4, 5, 6)]
		[TestCase(3.2, 4.8, 2.58, 3.831630878620748)]
		public void Triangle_GetSquare_ShouldReturnCorrectResult(double a, double b, double c, double result)
		{
			// Arrange
			var triangle = new Triangle(a, b, c);
			// Act & Assert
			Assert.AreEqual(result, triangle.GetSquare());
		}

		[Test]
		[TestCase(3, 4, 5, true)]
		[TestCase(3.2, 4.8, 2.58, false)]
		public void Triangle_IsRectangular_ShouldReturnCorrectResult(double a, double b, double c, bool result)
		{
			// Arrange
			var triangle = new Triangle(a, b, c);
			// Act & Assert
			Assert.AreEqual(result, triangle.IsRectangular());
		}
	}
}
