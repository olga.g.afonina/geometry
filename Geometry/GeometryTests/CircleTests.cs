using Figures;
using NUnit.Framework;
using System;

namespace GeometryTests
{
	public class CircleTests
	{
		[SetUp]
		public void Setup()
		{
		}

		[Test]
		[TestCase(-5.2)]
		[TestCase(0)]
		public void Circle__WhenRadiusIsLessOrEqualZero_ShouldThrowArgumentOutOfRange(double radius)
		{
			Assert.Throws<ArgumentOutOfRangeException>(() => new Circle(radius));
		}

		[Test]
		[TestCase(1, 3.141592653589793)]
		[TestCase(3.27, 33.5927360855703)]
		public void Circle_GetSquare_ShouldReturnCorrectResult(double radius, double result)
		{
			// Arrange
			var circle = new Circle(radius);
			// Act & Assert
			Assert.AreEqual(result, circle.GetSquare());
		}
	}
}